# Ejemplo de búsqueda DNS

La búsqueda es para encontrar `redes-ciencias-unam.gitlab.io`

### Búsqueda recursiva

La consulta es:

```
$ dig redes-ciencias-unam.gitlab.io.

; <<>> DiG 9.11.2-P1 <<>> redes-ciencias-unam.gitlab.io.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49693
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 7

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;redes-ciencias-unam.gitlab.io. IN      A

;; ANSWER SECTION:
redes-ciencias-unam.gitlab.io. 282 IN   A       52.167.214.135

;; AUTHORITY SECTION:
gitlab.io.              86059   IN      NS      ns-926.awsdns-51.net.
gitlab.io.              86059   IN      NS      ns-1697.awsdns-20.co.uk.
gitlab.io.              86059   IN      NS      ns-1116.awsdns-11.org.
gitlab.io.              86059   IN      NS      ns-288.awsdns-36.com.

;; ADDITIONAL SECTION:
ns-288.awsdns-36.com.   57141   IN      A       205.251.193.32
ns-288.awsdns-36.com.   57140   IN      AAAA    2600:9000:5301:2000::1
ns-926.awsdns-51.net.   66213   IN      A       205.251.195.158
ns-926.awsdns-51.net.   59295   IN      AAAA    2600:9000:5303:9e00::1
ns-1697.awsdns-20.co.uk. 60017  IN      A       205.251.198.161
ns-1697.awsdns-20.co.uk. 57072  IN      AAAA    2600:9000:5306:a100::1

;; Query time: 0 msec
;; SERVER: 192.168.254.205#53(192.168.254.205)
;; WHEN: Thu Feb 22 16:21:38 CST 2018
;; MSG SIZE  rcvd: 346
```

La parte que interesa es:
```
;; ANSWER SECTION:
redes-ciencias-unam.gitlab.io. 282 IN   A       52.167.214.135
```
que dice que la dirección es `52.167.214.135`.

## Búsqueda iterativa

Preguntamos por la raíz:

```
$ dig NS .

; <<>> DiG 9.11.2-P1 <<>> NS .
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40269
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 27

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;.                              IN      NS

;; ANSWER SECTION:
.                       407735  IN      NS      k.root-servers.net.
.                       407735  IN      NS      a.root-servers.net.
.                       407735  IN      NS      d.root-servers.net.
.                       407735  IN      NS      h.root-servers.net.
.                       407735  IN      NS      e.root-servers.net.
.                       407735  IN      NS      j.root-servers.net.
.                       407735  IN      NS      c.root-servers.net.
.                       407735  IN      NS      i.root-servers.net.
.                       407735  IN      NS      l.root-servers.net.
.                       407735  IN      NS      f.root-servers.net.
.                       407735  IN      NS      b.root-servers.net.
.                       407735  IN      NS      m.root-servers.net.
.                       407735  IN      NS      g.root-servers.net.

;; ADDITIONAL SECTION:
m.root-servers.net.     172019  IN      A       202.12.27.33
m.root-servers.net.     172019  IN      AAAA    2001:dc3::35
b.root-servers.net.     172019  IN      A       199.9.14.201
b.root-servers.net.     172019  IN      AAAA    2001:500:200::b
c.root-servers.net.     172019  IN      A       192.33.4.12
c.root-servers.net.     172019  IN      AAAA    2001:500:2::c
d.root-servers.net.     172019  IN      A       199.7.91.13
d.root-servers.net.     172019  IN      AAAA    2001:500:2d::d
e.root-servers.net.     172019  IN      A       192.203.230.10
e.root-servers.net.     172019  IN      AAAA    2001:500:a8::e
f.root-servers.net.     172019  IN      A       192.5.5.241
f.root-servers.net.     172019  IN      AAAA    2001:500:2f::f
g.root-servers.net.     172019  IN      A       192.112.36.4
g.root-servers.net.     172019  IN      AAAA    2001:500:12::d0d
h.root-servers.net.     172019  IN      A       198.97.190.53
h.root-servers.net.     172019  IN      AAAA    2001:500:1::53
i.root-servers.net.     172019  IN      A       192.36.148.17
i.root-servers.net.     172019  IN      AAAA    2001:7fe::53
a.root-servers.net.     195586  IN      A       198.41.0.4
a.root-servers.net.     604020  IN      AAAA    2001:503:ba3e::2:30
j.root-servers.net.     172019  IN      A       192.58.128.30
j.root-servers.net.     172019  IN      AAAA    2001:503:c27::2:30
k.root-servers.net.     172019  IN      A       193.0.14.129
k.root-servers.net.     172019  IN      AAAA    2001:7fd::1
l.root-servers.net.     172019  IN      A       199.7.83.42
l.root-servers.net.     172019  IN      AAAA    2001:500:9f::42

;; Query time: 0 msec
;; SERVER: 192.168.254.205#53(192.168.254.205)
;; WHEN: Thu Feb 22 16:37:05 CST 2018
;; MSG SIZE  rcvd: 811
```

La parte que nos interesa es la sección de respuesta. Elegimos un
servidor arbitrariamente:
```
;; ANSWER SECTION:
.                       407735  IN      NS      a.root-servers.net.
```

Ya que tenemos la raíz, preguntamos por `io.` al servidor `a.root-servers.net.`:

```
$ dig NS io. @a.root-servers.net.

; <<>> DiG 9.11.2-P1 <<>> NS io. @a.root-servers.net.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50941
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 6, ADDITIONAL: 12
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;io.                            IN      NS

;; AUTHORITY SECTION:
io.                     172800  IN      NS      a0.nic.io.
io.                     172800  IN      NS      a2.nic.io.
io.                     172800  IN      NS      b0.nic.io.
io.                     172800  IN      NS      c0.nic.io.
io.                     172800  IN      NS      ns-a1.io.
io.                     172800  IN      NS      ns-a3.io.

;; ADDITIONAL SECTION:
a0.nic.io.              172800  IN      A       65.22.160.17
a2.nic.io.              172800  IN      A       65.22.163.17
b0.nic.io.              172800  IN      A       65.22.161.17
c0.nic.io.              172800  IN      A       65.22.162.17
ns-a1.io.               172800  IN      A       194.0.1.1
ns-a3.io.               172800  IN      A       74.116.178.1
a0.nic.io.              172800  IN      AAAA    2a01:8840:9e::17
a2.nic.io.              172800  IN      AAAA    2a01:8840:a1::17
b0.nic.io.              172800  IN      AAAA    2a01:8840:9f::17
c0.nic.io.              172800  IN      AAAA    2a01:8840:a0::17
ns-a1.io.               172800  IN      AAAA    2001:678:4::1

;; Query time: 168 msec
;; SERVER: 198.41.0.4#53(198.41.0.4)
;; WHEN: Thu Feb 22 16:38:46 CST 2018
;; MSG SIZE  rcvd: 379
```

Tomamos la siguiente parte:
```
;; AUTHORITY SECTION:
io.                     172800  IN      NS      a0.nic.io.
```

Ahora preguntamos por `gitlab.io.` al servidor `a0.nic.io.`:
```
$ dig NS gitlab.io. @a0.nic.io.

; <<>> DiG 9.11.2-P1 <<>> NS gitlab.io. @a0.nic.io.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7931
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 4, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;gitlab.io.                     IN      NS

;; AUTHORITY SECTION:
gitlab.io.              86400   IN      NS      ns-926.awsdns-51.net.
gitlab.io.              86400   IN      NS      ns-288.awsdns-36.com.
gitlab.io.              86400   IN      NS      ns-1697.awsdns-20.co.uk.
gitlab.io.              86400   IN      NS      ns-1116.awsdns-11.org.

;; Query time: 57 msec
;; SERVER: 65.22.160.17#53(65.22.160.17)
;; WHEN: Thu Feb 22 16:39:43 CST 2018
;; MSG SIZE  rcvd: 178
```

Tomamos un servidor de la sección de autoridad:
```
;; AUTHORITY SECTION:
gitlab.io.              86400   IN      NS      ns-926.awsdns-51.net.
```

Le preguntamos a `ns-926.awsdns-51.net.` por el servidor DNS para `redes-ciencias-unam.gitlab.io.`:

```
$ dig NS redes-ciencias-unam.gitlab.io. @ns-926.awsdns-51.net.

; <<>> DiG 9.11.2-P1 <<>> NS redes-ciencias-unam.gitlab.io. @ns-926.awsdns-51.net.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20949
;; flags: qr aa rd; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;redes-ciencias-unam.gitlab.io. IN      NS

;; AUTHORITY SECTION:
gitlab.io.              900     IN      SOA     ns-1697.awsdns-20.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400

;; Query time: 45 msec
;; SERVER: 205.251.195.158#53(205.251.195.158)
;; WHEN: Thu Feb 22 16:40:23 CST 2018
;; MSG SIZE  rcvd: 145
```

De la respuesta:
```
;; AUTHORITY SECTION:
gitlab.io.              900     IN      SOA     ns-1697.awsdns-20.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400
```
tomamos `ns-1697.awsdns-20.co.uk.` y le preguntamos por la dirección de `redes-ciencias-unam.gitlab.io.`:

```
$ dig A redes-ciencias-unam.gitlab.io. @ns-1697.awsdns-20.co.uk.

; <<>> DiG 9.11.2-P1 <<>> A redes-ciencias-unam.gitlab.io. @ns-1697.awsdns-20.co.uk.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63150
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;redes-ciencias-unam.gitlab.io. IN      A

;; ANSWER SECTION:
redes-ciencias-unam.gitlab.io. 300 IN   A       52.167.214.135

;; AUTHORITY SECTION:
gitlab.io.              172800  IN      NS      ns-1116.awsdns-11.org.
gitlab.io.              172800  IN      NS      ns-1697.awsdns-20.co.uk.
gitlab.io.              172800  IN      NS      ns-288.awsdns-36.com.
gitlab.io.              172800  IN      NS      ns-926.awsdns-51.net.

;; Query time: 158 msec
;; SERVER: 205.251.198.161#53(205.251.198.161)
;; WHEN: Thu Feb 22 16:43:57 CST 2018
;; MSG SIZE  rcvd: 214
```

La respuesta fue:
```
;; ANSWER SECTION:
redes-ciencias-unam.gitlab.io. 300 IN   A       52.167.214.135
```
que nos dice que la dirección es `52.167.214.135` y es la misma que dio la búsqueda iterativa.
