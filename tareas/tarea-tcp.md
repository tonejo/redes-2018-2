# Tarea TCP

No encontré ningún problema.
Usé los comandos indicados casi sin modificación; los únicos cambios fueron:

  * Reemplazar el nombre de dominio `www.unam.mx` (`www_unam_mx`) por `gitlab.com` (`gitlab_com`)

  * Envolver entre comillas las opciones de `tcpdump` que incluían al carácter `#`.
    Hice esto porque el *shell* que utilizo (zsh) interpreta este carácter de forma especial.

  * Cambiar las opciones de `curl` de `-v#` a `-vsS`.
    Esto fue para evitar que el archivo resultado incluyera los indicadores de progreso.

Los comandos usados fueron:

```sh
dig +short gitlab.com | tee ip.txt
nc -vz gitlab.com 80

sudo tcpdump -n -w gitlab_com.pcap 'host gitlab.com and port 80'
curl -vsS 'http://gitlab.com/' -o gitlab_com.http.html | tee gitlab_com.http.log
# terminar tcpdump

sudo tcpdump '-#envr' gitlab_com.pcap 2>&1 | tee gitlab_com.pcap.txt
```

A diferencia de `http://www.unam.mx`, `http://gitlab.com` no regresa una página con el error; sólo da el código 301 y la nueva ubicación.
Como consecuencia de esto, el archivo `gitlab_com.http.html` está vacío.

## Pasos de la comunicación

Los primeros tres paquetes fueron para establecer la comunicación:

  1. Cliente > Servidor: `SYN`

  2. Servidor > Cliente: `SYN,ACK`

  3. Cliente > Servidor: `ACK`

Luego el cliente envió la petición de HTTP, repitiendo el `ACK` anterior

  4. Cliente > Servidor: `ACK`, `GET / HTTP/1.1` (también incluyó la bandera `PSH`)

El siguiente paquete es interesante. En el mismo paquete el servidor envía un `ACK` de la petición, responde a la petición de HTTP y cierra la conexión.

  5. Servidor > Cliente: `FIN,ACK`, `HTTP/1.1 301`.

Por último, el cliente también cierra la conexión.

  6. Cliente > Servidor: `FIN,ACK`

  7. Servidor > Cliente: `ACK`
