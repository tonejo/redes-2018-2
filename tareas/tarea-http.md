# Tarea HTTP

Notas sobre lo que hice en la tarea de HTTP:

  * No se generó ninguna cookie al usar `curl`. `wkhtmltoimage` sí
    generó _cookies_.

  * Usé la dirección `http://www.gitlab.com`, que redirigía a
    `https://gitlab.com` (permanentemente, código 301) y esta a su vez
    redirigía a `https://about.gitlab.com` (código 302).

## Respuestas a las preguntas del punto 5

  * La única diferencia entre los archivos HTML que se obtuvieron
    haciendo la consulta en curl y en el navegador fue una línea con
    un token CSRF. Los archivos que se obtuvieron usando curl con y
    sin las _cookies_ del navegador son completamente diferentes. Sin
    _cookies_ muestra la página principal, y con la cookie de sesión
    es la página del usuario con el que inicié sesión.

  * Además del archivo HTML que responde y cosas obvias como la fecha,
    sí cambia la respuesta del servidor cuando se le pasa la
    cookie. En este caso, la principal diferencia fue que sin cookie
    el servidor respondía con una redirección (de `https://gitlab.com`
    a `https://about.gitlab.com`) mientras que con la cookie de sesión
    respondía directamente con un archivo.

  * Se tienen que enviar _cookies_ al servidor porque HTTP es un
    protocolo sin estado pero queremos que el servidor "recuerde"
    quienes somos cuando hacemos una petición para que responda
    información personalizada.

  * Según `man curl`, la opción `--compressed` hace que se solicite al
    servidor una versión comprimida del recurso y guarde la versión
    descomprimida. La diferencia que podríamos ver si se dejara es que
    el mensaje enviado al servidor incluiría un encabezado
    `Accept-Encoding` indicando los formatos de compresión que se
    aceptan. Siendo así, no sé por qué se quitó la opción.

  * La cabecera `Accept` se cambió para garantizar que la respuesta
    recibida fuera HTML y no algún otro formato.
