`openssl verify` regresó un error `unable to get issuer certificate`
cuando se usó con la opción `-trusted`, pero dio `OK` con la opción
`-untrusted`.

Para validar el certificado del servidor, lo que se hace es recorrer
la cadena dada por quién es el _issuer_ de cada certificado y en cada
paso verificar que sea válido y se tenga el siguiente. El último
certificado de esta cadena debe estar en el directorio indicado por
`-CApath`, que contiene los certificados raíz en los que se confía. El
uso de `chain.pem` es dar la cadena que conecta el certificado del
servidor con un certificado raíz.

Si se usa la opción `-trusted`, la verificación debe poder llegar
hasta un certificado raíz. Si se usa `-untrusted`, sólo se verifica
que el _issuer_ del certificado que nos interesa (el del servidor)
esté en el archivo dado.
